# K22 cluster bootstrap

## Procedure

### Install ArgoCD

*TBA*

### Configure bootsrap Application

```bash
kubectl apply -f bootstrap-application.yml
```

## Description

Entry-point: `./bootstrap-application.yml`, an ArgoCD Application CR ("Argo App") that syncs all manifests inside `./cluster-manifests`.

These manifests includes cluster-wide configuration and other Argo Apps:
- `bootstrap-legacy.yml` - raw manifests syncing from `./legacy-deployments` with apps from previous clusters
- `bootstrap-charts.yml` - app of apps, one for each cluster-wide Helm Chart